import nunjucks from 'nunjucks';

export default ({types: t}) => {
    return {
        visitor: {
            Identifier(path, state) {
                for (let i = 0; i < state.opts.varNames.length; i += 1) {
                    if (
                        path.parentPath.type === 'VariableDeclarator' &&
                        path.isIdentifier({name: state.opts.varNames[i]})
                    ) {
                        const templatesFolder = state.opts.templatesFolder;
                        const value = path.parentPath.get('init').node.quasis[0].value.raw;
                        const env = new nunjucks.Environment(new nunjucks.FileSystemLoader(templatesFolder));
                        const render = env.renderString(value);
                        path.parentPath.get('init').node.quasis = [t.templateElement({cooked: render, raw: render})];
                        break;
                    }
                }
            },
        },
    };
};
