# babel-plugin-nunjucks

Плагин для [babel-loader](https://github.com/babel/babel-loader), позволяющий использовать шаблонизатор [Nunjucks](https://mozilla.github.io/nunjucks/) внутри JS-файлов.

## Установка

```bash
yarn add -D @advdominion/babel-plugin-nunjucks
```

## Использование

Код для преобразования должен находиться в переменных, имена которых указываются в массиве `varNames`, и быть [шаблонным литералом](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/template_strings). Пример:

```js
const nunjucksTemplate = `
    {% from "./item.njk" import item as item %}

    <div class="items">
		{% for n in range(0, 10) %}
			{{item()}}
		{% endfor %}
    </div>
`;
```

Пример конфигурации Webpack:

```js
[
    {
        test: /\.js$/,
        exclude: [/mocks\.js$/],
        use: [
            {
                loader: 'babel-loader',
                options: {
                    cacheDirectory: true,
                },
            },
        ],
    },
    {
        test: /mocks\.js$/,
        use: [
            {
                loader: 'babel-loader',
                options: {
                    compact: false,
                    plugins: [
                        [
                            '@advdominion/babel-plugin-nunjucks',
                            {
                                templatesFolder: 'src/templates/',
                                varNames: ['nunjucksTemplate'],
                            },
                        ],
                    ],
                },
            },
        ],
    },
];
```

## Опции

-   `templatesFolder` - обязательный параметр, путь до папки с файлами шаблонов.
-   `varNames` - Массив, обязательный параметр. Имена переменных для преобразования кода шаблонизатора.

**Важно:** Параметр `cacheDirectory` в опциях babel-loader должен быть отключён.
